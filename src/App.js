import './App.css';
import React from 'react';

import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Homepage from './contents/abstract/homepage';
import Login from './contents/user/login';
import Register from './contents/user/register';
import Listpage from './contents/abstract/listpage';
import Userspage from './contents/abstract/userspage';
import Addtask from './contents/abstract/addTask';
import Detailpage from './contents/abstract/detailpage';
import EditTask from './contents/abstract/editTask';
import EditUser from './contents/abstract/edituser';

function App() {
  return (
    // <Homepage />
    <BrowserRouter>
      <Routes>
        {/* <Route path='/' exact render={(props) => (
          <div>
            <Homepage />
          </div>
        )}/> */}
        <Route path='/' element={ <Homepage /> } />

        <Route path='/login'  element={ <Login />} />
        <Route path='/register' element={ <Register />} />
        <Route path='/listpage' element={ <Listpage />} />
        <Route path='/useraccount' element={ <Userspage />} />
        <Route path='/addtask' element={ <Addtask />} />
        <Route path='/detailpage' element={ <Detailpage />} />
        <Route path='/editTast/:id' element={ <EditTask />} />
        <Route path='/detailpage/:id' element={ <Detailpage />} />
        <Route path='/edituser/:id' element={ <EditUser />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
