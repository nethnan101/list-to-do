import axios from 'axios';
import React, { useState, useContext } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Context } from './Context';

export default function Login() {

    // const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    let navigate = useNavigate();

    const loginUsetoAPI = async (datas) => {
        const res = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/login', datas)
            .then(function (response) {
                return response.data;
            });
        return res; 
    }

    const { dispatch, isFetching, error } = useContext(Context);

    const onSubmitTask = async (e) =>{
            e.preventDefault();
            if(!email){
                alert('Please enter your gmail!');
                return;
            }

            if(!password){
                alert('Please enter your password!');
                return;
            }

            var resultLogin = await loginUsetoAPI( {email, password} );
            setEmail('');
            setPassword('');

            if(resultLogin) {
                dispatch({ type: "LOGIN_SUCESS", payload: resultLogin });
                navigate('/listpage');
            } else {
                dispatch({type: "LOGIN_FAILURE"});
            }
        }

    return (
        <>
        <div className="container">
            <h4>Login ToDo</h4>
            <form onSubmit={onSubmitTask}>
                <div className='form-group'>
                    <input type="email" className='form-control' id='email' placeholder='Email' name="email" value={email} onChange={(e)=> setEmail(e.target.value)} />
                </div>
                {/* <div className='form-group'>
                    <input type="numberic" className='form-control' id='phone' placeholder='Phone Number' name="phone" value={phone} onChange={(e)=> setPhone(e.target.value)} />
                </div> */}
                <div className='form-group'>
                    <input type="password" className='form-control' id='password' placeholder='Password' name="password" value={password} onChange={(e)=> setPassword(e.target.value)} />
                </div>
                <br />
                <button type="submit" className='btn btn-primary'>Login</button><span> </span>
                <Link to="/register"><button className='btn btn-outline-primary'>Register for now</button></Link>
            </form>
        </div>
        </>
    )
}
