import { React, useState, useContext } from "react";
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Context } from "./Context";

export default function Register() {
	const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
	const [age, setAge] = useState('');
	let navigate = useNavigate();
	const { dispatch , user} = useContext(Context);

	const onSubmitTask = async (e) => {
        e.preventDefault();
        if(!name) {
            alert('Please enter the name!');
            return;
        }

        if(!email) {
            alert('Please enter the email!');
            return;
        }

        if(!password) {
            alert('Please enter the password!');
            return;
        }

		if(!age) {
            alert('Please enter the age!');
            return;
        }

        var resultNewUser = await registerNewUser( {name, email, password, age} );
        setName('');
        setAge('');
        setEmail('');
        setPassword('');

		if(resultNewUser.token) {
			dispatch({ type: "LOGIN_SUCESS", payload: resultNewUser });
			navigate('/login');
		} else {
			dispatch({type: "LOGIN_FAILURE"});
		}
    }

	const registerNewUser = async (datas) => {
        const res = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/register', datas)
            .then(function (response) {
        	return response.data;
        })
        return res;
    }


	return (
		<div className="container mt-3">
			<h2>Register form</h2>
			<form onSubmit={onSubmitTask}>
				<div className="mb-3 mt-3">
					<label htmlFor="name">Name:</label>
					<input type="text" className="form-control" id="name" placeholder="Enter name" name="name" value={name} onChange={ (e) => setName(e.target.value) } />
				</div>
				<div className="mb-3 mt-3">
					<label htmlFor="email">Email:</label>
					<input type="email" className="form-control" id="email" placeholder="Enter email" name="email" value={email} onChange={ (e) => setEmail(e.target.value) } />
				</div>
				<div className="mb-3">
					<label htmlFor="pwd">Password:</label>
					<input type="password" className="form-control" id="pwd" placeholder="Enter password" name="pswd" value={password} onChange={ (e) => setPassword(e.target.value) } />
				</div>
				<div className="mb-3 mt-3">
					<label htmlFor="age">Age:</label>
					<input type="text" className="form-control" id="age" placeholder="Enter age" name="age" value={age} onChange={ (e) => setAge(e.target.value) } />
				</div>
				<button type="submit" className="btn btn-primary">Register</button> <span> </span>
				<Link to='/login'><button className="btn btn-outline-primary">Login</button></Link>
			</form>
		</div>
	)
}
