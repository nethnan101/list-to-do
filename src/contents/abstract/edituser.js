import React, { useContext, useEffect, useState} from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Context } from '../user/Context';
import axios from 'axios';

export default function EditUser() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
	const [age, setAge] = useState('');
	const { user, dispatch} = useContext(Context);
	let navigate = useNavigate();
	const [ profile, serProfile] = useState([]);
    let {id} = useParams();
    console.log(user);
    user.user.name = "Syden";
    console.log(user);
	const onEditUser = async (e) => {
        e.preventDefault();
        console.log(e);
        var updateNewUser = await NewUser({name, age});
        console.log("updateNewUser");
        console.log(updateNewUser);
        
        if(updateNewUser){          
            user.user.name = updateNewUser.data.name;
            user.user.age = updateNewUser.data.age;
            dispatch({payload: user });
            navigate('/useraccount')
        }
    }
    const NewUser = async (datas) => {
        const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}
        console.log(headerRe);
        const res = await axios.put('https://api-nodejs-todolist.herokuapp.com/user/me',datas, headerRe)
        .then((res) => {
            return res.data;
        })
        return res;
    }

    const fetchUser = async (datas) => {
        const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}
        const res = await axios.get('https://api-nodejs-todolist.herokuapp.com/user/me', headerRe)
            .then((res) => {
                return res.data;
            });
        return res;
    }
    useEffect (() => {
        const getCate = async (data) => {
            const UserFromAPI = await fetchUser(data)
            setName(UserFromAPI.name);
            // setEmail(UserFromAPI.email);
            setAge(UserFromAPI.age);
        }
        getCate(id)
    },[id])

	return (
		<>
			<div className='container'>
				<h2 className='text-center'>Edit Your Account</h2>
				<p className='text-center'>Todo App</p>
			</div>
			<div className="container rounded">
                <form onSubmit={onEditUser}>
                    <div className="mb-3 mt-3">
                        <label htmlFor="name">Name:</label>
                        <input type="text" className="form-control" id="name" placeholder="Enter name" name="name" value={name} onChange={ (e) => setName(e.target.value) } />
                    </div>
                    {/* <div className="mb-3 mt-3">
                        <label htmlFor="email">Email:</label>
                        <input type="email" className="form-control" id="email" placeholder="Enter email" name="email" value={email} onChange={ (e) => setEmail(e.target.value) } />
                    </div> */}
                    <div className="mb-3 mt-3">
                        <label htmlFor="age">Age:</label>
                        <input type="text" className="form-control" id="age" placeholder="Enter age" name="age" value={age} onChange={ (e) => setAge(e.target.value) } />
                    </div>
                    <button type="submit" className="btn btn-primary">Save</button> <span> </span>
                    <Link to='/useraccount'><button className="btn btn-outline-primary">Cencel</button></Link>
                </form>
			</div>
		</>
	)
}
