import React from 'react'

import { Link } from 'react-router-dom'

export default function Homepage() {
  return (
    <div className="homepage">
        <div className='container'>
            <h1 className='text-light'>Todo List</h1>
        </div>
        <div className="container button-homepage">         
            <Link to='/login'>
                <button type='button' className='btn btn-outline-primary'>Login</button>
            </Link>
            <span> </span>
            <Link to='/register'>
                <button type='button' className='btn btn-outline-success'>Register</button>
            </Link>
        </div>
    </div>
  )
}