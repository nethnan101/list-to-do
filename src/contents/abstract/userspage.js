import React, { useContext, useEffect, useState} from 'react';
import { Link, useNavigate} from 'react-router-dom';
import { Context } from '../user/Context';
import axios from 'axios';

export default function Userspage() {

	const { user, dispatch} = useContext(Context);
	let navigate = useNavigate();
	const [ profile, serProfile] = useState([]);

	const Logout = async (datas) => {
		const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}
		const res = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/logout', "" ,headerRe)
			.then(function (response) {
				console.log("response");
				console.log(response);
				return response.data;
			})
		
		if(res.success){
			dispatch({ type: "LOGOUT", payload: {} });
			navigate('/');
		}
	}

	const goToEditUser = (_id) => {
		navigate('/edituser/' + _id)
	}

	return (
		<>
			<div className='container'>
				<Link to='/addtask' className=''><button className='btn btn-primary'>ToDo</button></Link>
				<Link onClick={Logout} to=''><button className='btn btn-outline-danger'>Log out</button></Link>
				<h2 className='text-center'>Your Account</h2>
				<p className='text-center'>Todo App</p>
			</div>
			<div className="container border border-danger rounded">
				<table className="table table-hover">
					<tbody>
						<tr>
							<th scope="row">Name:</th>
							<td>{user.user.name}</td>
						</tr>
						<tr>
							<th scope="row">Email</th>
							<td>{user.user.email}</td>
						</tr>
						<tr>
							<th scope="row">Age</th>
							<td>{user.user.age}</td>
						</tr>
						<button onClick={() => goToEditUser(user.user._id)} className='btn btn-outline-primary'>Edit Profile</button>
					</tbody>
				</table>
			</div>
		</>
	)
}
