import React, {useContext, useEffect, useState} from 'react';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import { Context } from '../user/Context';

export default function Detailpage() {

    const [description, setDescription] = useState('');
    const {user} = useContext(Context);
    let{ id } = useParams();
    console.log("id");
    console.log(id);

    const fetchTaskById = async (datas) => {
        console.log("id");
        console.log(id);
        const headerRe = {
    headers: {
        Authorization: 'Bearer ' + user.token
    }
}
    console.log(headerRe);
    const res = await axios.get('https://api-nodejs-todolist.herokuapp.com/task/'+`${id}`, headerRe)
        .then((res) => {
            console.log(res);
            return res.data;
        });
    console.log(res);
    return res;
}
useEffect (() => {
    const getCate = async (data) => {
        const TaskFromAPI = await fetchTaskById(data)
        setDescription(TaskFromAPI.data.description)
    }
    getCate(id)
},[id])

  return (
    <>
        <div className="container">
            <h3 className='text-center'>Data</h3>
            <Link to='/listpage'><button className='btn btn-outline-danger'>Go Back</button></Link>
            <div className="border border-info rounded">
                <div className="container">
                    <h4>Your description</h4>
                    <p>Description: <span className='btn-primary'>{description}</span></p>
                </div>
            </div>
        </div>
    </>
  )
}
