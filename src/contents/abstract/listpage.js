import React, { useState, useContext, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Context } from '../user/Context';
import { BiMessageSquareDetail } from 'react-icons/bi';
import { RiDeleteBin6Line , RiEdit2Line } from 'react-icons/ri';
import Swal from 'sweetalert2';

export default function Listpage({description}) {

	const [listdes, setListdes] = useState([]);
	const { user } = useContext(Context);
	let navigate = useNavigate();

	// const getData = () => {
	// 	fetch()
	// }

	useEffect (() => {
        const fetchDescription = async (description) => {
			const headerRe = {
				headers: {
					Authorization: 'Bearer ' + user.token
				}
			}
            const resu = await axios.get('https://api-nodejs-todolist.herokuapp.com/task', headerRe)
            .then(function (response) {
                console.log(response.data.data);
                return response.data.data;
            });
            setListdes(resu);
        }
        fetchDescription(description);        
    },[description]);

	const deleteUsers = async (id) => {

		// console.log("Data");
		// console.log(datas);

		const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}

        Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this data!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then( async (result) => {
            if (result.isConfirmed) {
                const result = await axios.delete('https://api-nodejs-todolist.herokuapp.com/task/'+`${id}`, headerRe)                         
            }
        })
    }

	const goToEdit = (_id) =>{
		console.log(_id);
		navigate('/editTast/' + _id)
	}

    const goToDetail = (_id) => {
        console.log(_id);
        navigate('/detailpage/' + _id)
    }

    return (
        <>
			<div className="container">
				<h2>Todo List</h2>
				<Link to='/addtask'><button className='btn btn-outline-danger'>Add more</button></Link>
                <Link to='/useraccount'><button className='btn btn-outline-danger'>Account</button></Link>
			</div>
			<div className="container">                    
                <table className="table">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Despcription</th>
                            <th>Status</th>
							<th>Action</th>
							<th>_id</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listdes.length > 0 ? (
                            <>
                                {listdes.map((listdes) => (
                                    <tr key={listdes.id}>
										<td>{listdes.createdAt}</td>
                                        <td>{listdes.description}</td>
                                        <td>
                                            {listdes.completed ? "Completed" : "Pending"}
                                        </td>
										<td>{ <RiEdit2Line onClick={() => goToEdit(listdes._id)}/>} 
                                            { <RiDeleteBin6Line onClick={() => deleteUsers(listdes._id)}/>} 
                                            { <BiMessageSquareDetail onClick={() => goToDetail(listdes._id)}/>}
                                        </td>
										<td>{listdes._id}</td>
										{/* <td>{listdes.listdes}</td>
                                        <td>{listdes.order}</td> */}
                                        {/* <td>
                                            <Link to={"/listdes/" + category._id } className="btn btn-primary m-1">Edit</Link>
                                            <Link to="/category" onClick={() => deleteUsers(category._id)} className="btn btn-danger m-1">Delete</Link>
                                        </td>                                 */}
                                    </tr>

                                ))}
                            </>
                        ) : (
                            <>
                                <tr>
                                    <td colSpan="4">There are no record!</td>
                                </tr>
                            </>
                        ) }
                    </tbody>
                </table>
            </div>
        </>
    )
}
