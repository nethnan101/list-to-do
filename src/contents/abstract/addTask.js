import React, { useState, useContext } from 'react'
import axios from 'axios';
import { Context } from '../user/Context';
import {Link ,useNavigate } from 'react-router-dom';

const Addtask = ({ onAdd }) => {
    const [description, setDescription] = useState('');
    const [reminder, setReminder] = useState(false);
    const { user, dispatch} = useContext(Context);
    let navigate = useNavigate();

    const onSubmit = (e) => {
        e.preventDefault()
        if(!description) {
            alert('Enter your Bio')
            return
        }

        var onAdd = addTask ({ description});
        if(onAdd) {
            navigate('/listpage');
        }

        setDescription('')
    }

    const addTask = async (datas) => {
        const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}

		const res = await axios.post('https://api-nodejs-todolist.herokuapp.com/task', datas ,headerRe)
			.then(function (response) {
				return response.data;
		    })
    }

    return (
        <div className='containers'>
            <h2>Add Your Task</h2>
            <form className='add-form' onSubmit={onSubmit}>
                <div className='form-control'>
                    <label>Description</label>
                    <input
                        type='text'
                        placeholder='Description'
                        value={description} 
                        onChange={(e) => setDescription(e.target.value)} 
                    />
                </div>
                <input type='submit' value='Add' className='btn btn-block '/>
                <Link to='/listpage'><button className='btn btn-outline-danger'>Cencel</button></Link>
            </form>
        </div>
    )
}

export default Addtask