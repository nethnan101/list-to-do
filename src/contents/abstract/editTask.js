import axios from 'axios';
import React, { useState, useContext, useEffect } from 'react';
import { Link ,useNavigate, useParams } from 'react-router-dom';
import { Context } from '../user/Context';

export default function EditTask() {

    const [description, setDescription] = useState('');
    const { user } = useContext(Context);
    let navigate = useNavigate();
    let {id} = useParams();

    const onUpdate = async (e) => {
        e.preventDefault();
        var newTask = await UpdateTask({description}, id);
        if(newTask){
            navigate('/listpage')
        }
    }

    const UpdateTask = async (datas , id) => {
        // const updateNew = {description:datas.description};
        const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}
        const res = await axios.put('https://api-nodejs-todolist.herokuapp.com/task/'+`${id}` ,datas, headerRe)
        .then((res) => {
            return res.data;
        })
        return res;
    }

    const fetchTaskById = async (datas) => {
        const headerRe = {
			headers: {
				Authorization: 'Bearer ' + user.token
			}
		}
        console.log(headerRe);
        const res = await axios.get('https://api-nodejs-todolist.herokuapp.com/task/'+`${id}`, headerRe)
            .then((res) => {
                return res.data;
            });
        return res;
    }
    useEffect (() => {
        const getCate = async (data) => {
            const TaskFromAPI = await fetchTaskById(data)
            setDescription(TaskFromAPI.data.description)
        }
        getCate(id)
    },[id])

  return (
    <>
        <div className='containers'>
            <h2>Update Your Task</h2>
            <form className='add-form' onSubmit={onUpdate}>
                <div className='form-control'>
                    <label>Description</label>
                    <input
                        type='text'
                        placeholder='Description'
                        value={description} 
                        onChange={(e) => setDescription(e.target.value)} 
                    />
                </div>
                <input type='submit' value='Add' className='btn btn-block '/>
                <Link to='/listpage'><button className='btn btn-outline-danger'>Cencel</button></Link>
            </form>
        </div>
    </>
  )
}
